//
// Created by Jakub Begera on 02.05.17.
//

#include "Server.h"

Server::Server() {}

int Server::start(int port) {
    struct MHD_Daemon *server = MHD_start_daemon(MHD_USE_THREAD_PER_CONNECTION, port, NULL, NULL, &processRequest, NULL,
                                                 MHD_OPTION_NOTIFY_COMPLETED, &onRequestCompleted, NULL,
                                                 MHD_OPTION_END);
    if (server == NULL) {
        fprintf(stderr, "Server starting was failed, system will exit... :-(\n");
        return 1;
    }
    fprintf(stdout, "Server started. Listening on port %d\n", port);
    fprintf(stdout, "Type \'s\' for stop \n");

    int ch;
    while ((ch = getchar()) != 0) {
        if ('s' == ch) {
            fprintf(stdout, "Server is shutting down...\n");
            MHD_stop_daemon(server);
            return 0;
        }
    }
    return 0;
}

int Server::processRequest(void *cls, struct MHD_Connection *connection, const char *url, const char *method,
                           const char *version, const char *upload_data, size_t *upload_data_size, void **con_cls) {
    // get header
    if (*con_cls == NULL) {
        struct con_params *con_p;
        con_p = (con_params *) malloc(sizeof(struct con_params));
        if (con_p == NULL) {
            return MHD_NO;
        } else {
            con_p->bodyIndex = 0;
            con_p->path = url;
            con_p->body = new char[1000000]();
            *con_cls = (void *) con_p;
            return MHD_YES;
        }
    }
    struct con_params *conp = (con_params *) *con_cls;

    // process one part of request data
    if (*upload_data_size > 0) {
        for (size_t i = 0; i < *upload_data_size; i++) {
            conp->body[conp->bodyIndex + i] = upload_data[i];
        }
        conp->bodyIndex += *upload_data_size;
        *upload_data_size = 0;
        return MHD_YES;
    }

    // delegate request to proper handler
    if (strcmp(conp->path, "/esw/myserver/count") == 0 && strcasecmp(method, MHD_HTTP_METHOD_GET) == 0) {
        return handleGetCountRequest(connection);
    } else if (strcmp(conp->path, "/esw/myserver/data") == 0 && strcasecmp(method, MHD_HTTP_METHOD_POST) == 0) {
        return handleDataAddRequest(conp, connection);
    } else {
        return sendResponse(connection, "BAD_REQUEST\n", MHD_HTTP_BAD_REQUEST);
    }
}

int Server::handleDataAddRequest(struct con_params *con_info, struct MHD_Connection *connection) {
    char *p;
    char *text = gzipUtils.decompress(con_info->body, con_info->bodyIndex);
    char *word = strtok_r(text, SEPARATOR, &p);
    do {
        storage.addWord(word);
    } while ((word = strtok_r(NULL, SEPARATOR, &p)) != NULL);
    return sendResponse(connection, "", MHD_HTTP_OK);
}

int Server::handleGetCountRequest(struct MHD_Connection *connection) {
    char result[256];
    sprintf(result, "%d\n", storage.getCountAndReset());
    return sendResponse(connection, result, MHD_HTTP_OK);
}

void Server::onRequestCompleted(void *cls, struct MHD_Connection *connection, void **con_cls,
                                enum MHD_RequestTerminationCode toe) {
    struct con_params *con_p = (con_params *) *con_cls;
    if (con_p != NULL) {
        if (con_p->body != NULL) {
            free(con_p->body);
        }
        free(con_p);
        *con_cls = NULL;
    }
}

int Server::sendResponse(struct MHD_Connection *conn, const char *responseBody, int statusCode) {
    struct MHD_Response *response = MHD_create_response_from_buffer(
            strlen(responseBody),
            (void *) responseBody,
            MHD_RESPMEM_MUST_COPY
    );
    if (response != NULL) {
        MHD_add_response_header(response, MHD_HTTP_HEADER_CONTENT_TYPE, "text/plain");
        int success = MHD_queue_response(conn, (unsigned int) statusCode, response);
        MHD_destroy_response(response);
        return success;
    }
    return MHD_NO;
}

