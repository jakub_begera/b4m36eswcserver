//
// Created by Jakub Begera on 02.05.17.
//

#ifndef ESWCSERVER_SERVER_H
#define ESWCSERVER_SERVER_H


#define SEPARATOR " \t\r\n"

#include <stdio.h>
#include <stdlib.h>
#include <microhttpd.h>
#include <stdexcept>
#include <string.h>
#include "WordStorage.h"
#include "GzipUtils.h"

using namespace std;

static WordStorage storage;
static GzipUtils gzipUtils;

class Server {
public:
    Server();

    int start(int port);

private:


    struct con_params {
        const char *path;
        char *body;
        int bodyIndex;
    };

    static int processRequest(void *cls, struct MHD_Connection *connection, const char *url, const char *method,
                              const char *version, const char *upload_data, size_t *upload_data_size, void **con_cls);

    static void onRequestCompleted(void *cls, struct MHD_Connection *connection, void **con_cls,
                                   enum MHD_RequestTerminationCode toe);

    static int handleDataAddRequest(struct con_params *con_info, struct MHD_Connection *connection);

    static int handleGetCountRequest(struct MHD_Connection *connection);

    static int sendResponse(struct MHD_Connection *conn, const char *responseBody, int statusCode);
};


#endif //ESWCSERVER_SERVER_H
