//
// Created by Jakub Begera on 02.05.17.
//

#include "Server.h"

int main(int argc, char *argv[]) {

    int port = atoi(argv[1]);

    Server server;

    return server.start(port);
}