//
// Created by Jakub Begera on 03.05.17.
//

#ifndef ESWCSERVER_WORDSTORAGE_H
#define ESWCSERVER_WORDSTORAGE_H

#include <string>
#include <unordered_set>
#include <pthread.h>

using namespace std;

class WordStorage {

public:
    WordStorage(void);

    void addWord(char *word);

    int getCountAndReset();

private:
    unordered_set<string> *set;
    pthread_mutex_t *wordStorageLock;

};


#endif //ESWCSERVER_WORDSTORAGE_H
