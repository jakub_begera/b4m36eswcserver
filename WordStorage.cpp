//
// Created by Jakub Begera on 03.05.17.
//

#include "WordStorage.h"

WordStorage::WordStorage(void) {
    set = new unordered_set<string>();
    wordStorageLock = new pthread_mutex_t();
};

void WordStorage::addWord(char *word) {
    if (set->count((const basic_string<char> &) word) != 1) {
        pthread_mutex_lock(wordStorageLock);
        set->insert((basic_string<char, char_traits<char>, allocator<char>> &&) word);
        pthread_mutex_unlock(wordStorageLock);
    }
}

int WordStorage::getCountAndReset() {
    unsigned long count = set->size();
    set->clear();
    return (int) count;
}

