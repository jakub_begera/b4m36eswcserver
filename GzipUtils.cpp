//
// Created by Jakub Begera on 02.05.17.
//

#include <zlib.h>
#include "GzipUtils.h"

GzipUtils::GzipUtils() {

}

char *GzipUtils::decompress(const char *content, int size) {
    char *out = new char[4 * size]();
    z_stream stream;
    stream.zfree = Z_NULL;
    stream.zalloc = Z_NULL;
    stream.opaque = Z_NULL;
    stream.avail_in = (uInt) size;
    stream.next_in = (Bytef *) content;
    stream.avail_out = (uInt) 4 * size;
    stream.next_out = (Bytef *) out;
    inflateInit2(&stream, 16 + MAX_WBITS);
    inflate(&stream, Z_NO_FLUSH);
    inflateEnd(&stream);
    return out;
}
