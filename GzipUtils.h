//
// Created by Jakub Begera on 02.05.17.
//

#ifndef ESWCSERVER_GZIPUTILS_H
#define ESWCSERVER_GZIPUTILS_H


class GzipUtils {
public:
    GzipUtils();

    char *decompress(const char *content, int size);

};


#endif //ESWCSERVER_GZIPUTILS_H
